package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.dto.BlogDTO;
import com.example.demo.model.Blog;
import com.example.demo.model.Category;
@Repository
public interface BlogRepository extends JpaRepository<Blog,Integer>{
	
	List<Blog> findAllByCatIdCatId(Long get);

	
	@Query("SELECT b FROM Blog b ORDER BY b.likes DESC LIMIT 4")
    List<Blog> findTop4MostPopularArticles();
	
	@Query("SELECT b FROM Blog b ORDER BY b.likes DESC ")
    List<Blog> findAllMostPopularArticles();
	
	@Query("SELECT b FROM Blog b WHERE b.catId = :catId ORDER BY b.likes DESC LIMIT 4")
    List<Blog> findTop4ByJavaOrderByLikesDesc(Category catId);
	
	@Query("SELECT b FROM Blog b WHERE b.catId = :catId ORDER BY b.likes DESC LIMIT 4")
	List<Blog> findTop4ByPythonOrderByLikesDesc(Category catId);
	
	@Query("SELECT b FROM Blog b WHERE b.catId = :catId ORDER BY b.likes DESC LIMIT 4")
	List<Blog> findTop4ByWebtechOrderByLikesDesc(Category catId);
	
	@Query("SELECT b FROM Blog b WHERE b.catId = :catId ORDER BY b.likes DESC LIMIT 4")
	List<Blog> findTop4ByDbOrderByLikesDesc(Category catId);
	
	@Query("SELECT b FROM Blog b WHERE b.catId = :catId ORDER BY b.likes DESC")
	List<Blog> findAllJavaOrderByLikesDesc(Category catId);
	
	@Query("SELECT b FROM Blog b WHERE b.catId = :catId ORDER BY b.likes DESC")
	List<Blog> findAllPythonOrderByLikesDesc(Category catId);
	
	@Query("SELECT b FROM Blog b WHERE b.catId = :catId ORDER BY b.likes DESC")
	List<Blog> findAllWebtechOrderByLikesDesc(Category catId);
	
	@Query("SELECT b FROM Blog b WHERE b.catId = :catId ORDER BY b.likes DESC")
	List<Blog> findAllDbOrderByLikesDesc(Category catId);
	
	List<Blog> findByBlogTitle(String blogTitle);
	
	List<Blog> findByUserIdId(int userId);
	
	Blog deleteById(int blogId);
	

	
}
