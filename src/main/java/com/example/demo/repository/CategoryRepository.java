package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Blog;
import com.example.demo.model.Category;
import java.util.List;



@Repository
public interface CategoryRepository extends JpaRepository<Category,Long> {

	Category findOneByCatId(Long catId);
}
