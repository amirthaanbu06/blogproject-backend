package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Blog;
import com.example.demo.model.UserTable;
@Repository
public interface UserRepository extends JpaRepository<UserTable, Integer>{
	
		UserTable findOneById(Integer Id);

    Optional<UserTable> findByEmail(String name);

}
