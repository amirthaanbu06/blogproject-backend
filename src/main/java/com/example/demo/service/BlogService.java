package com.example.demo.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.BlogDTO;
import com.example.demo.model.Blog;
import com.example.demo.model.Category;
import com.example.demo.model.UserTable;
import com.example.demo.model.responceVo;
import com.example.demo.repository.BlogRepository;
import com.example.demo.repository.CategoryRepository;
import com.example.demo.repository.UserRepository;

@Service
public class BlogService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BlogRepository blogRepository;

	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	
	public List<BlogDTO> getUserBlogs(int userId) {
        List<Blog> blogs = blogRepository.findByUserIdId(userId);

        // Use ModelMapper to convert Blog entities to BlogDTOs
        List<BlogDTO> blogDTOs = blogs.stream()
                .map(blog -> modelMapper.map(blog, BlogDTO.class))
                .collect(Collectors.toList());

        return blogDTOs;
    }

	public UserTable addRegisterService(UserTable reg) {
//		System.out.println(222222222);
		return this.userRepository.save(reg);
	}

	public responceVo logindata2(UserTable regdata) {

		responceVo responce = new responceVo();
		UserTable login = userRepository.findByEmail(regdata.getEmail()).orElse(null);
		if (login != null) {
			if (login.getPassword().equals(regdata.getPassword())) {
				responce.setUserId(login.getId());
				responce.setReturnResponce("Successfully");
				return responce;
			}
		}
		responce.setReturnResponce("PleaseRegister");
		return responce;
	}
//catId-java,python,webtech,db(1,2,3,4)
	public List<Blog> getBlog(Long get) {
		return blogRepository.findAllByCatIdCatId(get);
	}

	public Blog addBlogService(BlogDTO post, Integer userId) {

		// get call - usertable by userid
		UserTable userTable=userRepository.findOneById(userId);
		Integer catId =post.getCatId();
		Category cat = categoryRepository.findOneByCatId(Long.parseLong(catId.toString()));
		Blog saveBlog = new Blog();
		saveBlog.setAuthor(post.getAuthor());
		saveBlog.setBlogDescription(post.getBlogDescription());
		saveBlog.setBlogTitle(post.getBlogTitle());
		saveBlog.setCatId(cat);
		if(userTable !=null) {
			 saveBlog.setUserId(userTable);
		}
		saveBlog.setTimestamp(new Date());
		return this.blogRepository.save(saveBlog);
	}

	private String formatTimestamp(String timestamp) {
		try {
			SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat outputFormat = new SimpleDateFormat("MMM dd yyyy");
			Date date = inputFormat.parse(timestamp);
			return outputFormat.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return "Invalid Timestamp";
		}
	}

	private void commonFunc(List<BlogDTO> dtos, List<Blog> popularArticles) {
		// TODO Auto-generated method stub
		for (Blog blog : popularArticles) {
			String formattedTimestamp = formatTimestamp(blog.getTimestamp().toString());
			BlogDTO dto = new BlogDTO(blog.getBlogId(), blog.getBlogTitle(), blog.getBlogDescription(), blog.getLikes(),
					blog.getComments(), blog.getAuthor(), formattedTimestamp);
			dtos.add(dto);
		}

	}

	public List<BlogDTO> getTop4MostPopularArticles() {
//        return blogRepository.findTop4MostPopularArticles();

		List<Blog> popularArticles = blogRepository.findTop4MostPopularArticles();
		List<BlogDTO> dtos = new ArrayList<>();

		commonFunc(dtos, popularArticles);

		return dtos;
	}

	public List<BlogDTO> getAllMostPopularArticles() {
//      return blogRepository.findTop4MostPopularArticles();

		List<Blog> popularArticles = blogRepository.findAllMostPopularArticles();
		List<BlogDTO> dtos = new ArrayList<>();

		commonFunc(dtos, popularArticles);

		return dtos;
	}

	public List<BlogDTO> getTop4JavaBlogsByLikes() {
//        return blogRepository.findTop4ByJavaOrderByLikesDesc(1);
		Category cat=  categoryRepository.findOneByCatId(1L);

		List<Blog> popularArticles = blogRepository.findTop4ByJavaOrderByLikesDesc(cat);
		List<BlogDTO> dtos = new ArrayList<>();

		commonFunc(dtos, popularArticles);

		return dtos;
	}

	public List<BlogDTO> getTop4PythonBlogsByLikes() {
//		return blogRepository.findTop4ByPythonOrderByLikesDesc(2);
		Category cat=  categoryRepository.findOneByCatId(2L);

		List<Blog> popularArticles = blogRepository.findTop4ByPythonOrderByLikesDesc(cat);
		List<BlogDTO> dtos = new ArrayList<>();

		commonFunc(dtos, popularArticles);

		return dtos;
	}

	public List<BlogDTO> getTop4WebtechBlogsByLikes() {
//		return blogRepository.findTop4ByWebtechOrderByLikesDesc(3);
		Category cat=  categoryRepository.findOneByCatId(3L);

		List<Blog> popularArticles = blogRepository.findTop4ByWebtechOrderByLikesDesc(cat);
		List<BlogDTO> dtos = new ArrayList<>();

		commonFunc(dtos, popularArticles);

		return dtos;
	}

	public List<BlogDTO> getTop4DbBlogsByLikes() {
//		return blogRepository.findTop4ByDbOrderByLikesDesc(4);
		Category cat=  categoryRepository.findOneByCatId(4L);
		List<Blog> popularArticles = blogRepository.findTop4ByDbOrderByLikesDesc(cat);
		List<BlogDTO> dtos = new ArrayList<>();

		commonFunc(dtos, popularArticles);

		return dtos;
	}

	public List<BlogDTO> getAllJavaBlogsByLikes() {
//		return blogRepository.findAllJavaOrderByLikesDesc(1);
		Category cat=  categoryRepository.findOneByCatId(1L);

		List<Blog> popularArticles = blogRepository.findAllJavaOrderByLikesDesc(cat);
		List<BlogDTO> dtos = new ArrayList<>();

		commonFunc(dtos, popularArticles);

		return dtos;

	}

	public List<BlogDTO> getAllPythonBlogsByLikes() {
//		return blogRepository.findAllPythonOrderByLikesDesc(2);
		Category cat=  categoryRepository.findOneByCatId(2L);

		List<Blog> popularArticles = blogRepository.findAllPythonOrderByLikesDesc(cat);
		List<BlogDTO> dtos = new ArrayList<>();

		commonFunc(dtos, popularArticles);

		return dtos;
	}

	public List<BlogDTO> getAllWebtechBlogsByLikes() {
//		return blogRepository.findAllWebtechOrderByLikesDesc(3);
		Category cat=  categoryRepository.findOneByCatId(3L);

		List<Blog> popularArticles = blogRepository.findAllWebtechOrderByLikesDesc(cat);
		List<BlogDTO> dtos = new ArrayList<>();

		commonFunc(dtos, popularArticles);

		return dtos;

	}

	public List<BlogDTO> getAllDbBlogsByLikes() {
//		return blogRepository.findAllDbOrderByLikesDesc(4);
		Category cat=  categoryRepository.findOneByCatId(4L);

		List<Blog> popularArticles = blogRepository.findAllDbOrderByLikesDesc(cat);
		List<BlogDTO> dtos = new ArrayList<>();

		commonFunc(dtos, popularArticles);

		return dtos;
	}

	public List<Blog> getBlogsByTitle(String blogTitle) {
		return blogRepository.findByBlogTitle(blogTitle); 
	}

//	public List<BlogDTO> getUserBlogs(int userId) {
//		System.out.println(33333);
//		return blogRepository.findByUserIdId(userId);
//	}
	
	public Blog deleteMyBlog(int blogId) {
		return blogRepository.deleteById(blogId);
	}
}
