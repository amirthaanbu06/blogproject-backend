package com.example.demo.model;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "blog_tbl")
public class Blog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "blog_id")
    private int blogId;

    @Column(name = "blog_title")
    private String blogTitle;

    @Column(name = "blog_desc")
    private String blogDescription;

    @Column(name = "likes")
    private int likes;

    @Column(name = "comments")
    private int comments;
    
    @Column(name = "author")
    private String author;
    
    @Column(name = "timestamp")
    private Date timestamp;
    
    @ManyToOne(fetch=FetchType.LAZY,targetEntity=Category.class)
    @JoinColumn(name = "cat_id")
    private Category catId;
    
    @ManyToOne(fetch=FetchType.LAZY,targetEntity=UserTable.class)
    @JoinColumn(name = "user_id")
    private UserTable userId;

	public UserTable getUserId() {
		return userId;
	}

	public void setUserId(UserTable userId) {
		this.userId = userId;
	}

	public int getBlogId() {
		return blogId;
	}

	public void setBlogId(int blogId) {
		this.blogId = blogId;
	}

	public String getBlogTitle() {
		return blogTitle;
	}

	public void setBlogTitle(String blogTitle) {
		this.blogTitle = blogTitle;
	}

	public String getBlogDescription() {
		return blogDescription;
	}

	public void setBlogDescription(String blogDescription) {
		this.blogDescription = blogDescription;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public int getComments() {
		return comments;
	}

	public void setComments(int comments) {
		this.comments = comments;
	}

	public Category getCatId() {
		return catId;
	}

	public void setCatId(Category catId) {
		this.catId = catId;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}


	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

//	public Blog(int blogId, String blogTitle, String blogDescription, int likes, int comments, String author,
//			String timestamp, Category catId, UserTable userId) {
//		super();
//		this.blogId = blogId;
//		this.blogTitle = blogTitle;
//		this.blogDescription = blogDescription;
//		this.likes = likes;
//		this.comments = comments;
//		this.author = author;
//		this.timestamp = timestamp;
//		this.catId = catId;
//		this.userId = userId;
//	}

	
}
