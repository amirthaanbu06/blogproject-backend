package com.example.demo.model;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name="user_table")
public class UserTable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
//	@Column(name="id")
	private int id;
//	@Column(name="user_name")
	private String name;
//	@Column(name="user_email", nullable=false, unique=true)
	private String email;
//	@Column(name="user_password")
	private String password;
	@Column(name="contact_number")
	private String contactNumber;
//	@Column(name="user_age")
	private String age;
//	@Column(name="user_dob")
	private String dob;
//	@Column(name="user_address")
	private String address;
//	@Column(name="user_city")
	private String city;
//	@Column(name="user_state")
	private String state ;
//	@Column(name="user_profile")
	private String profile_pic;
//	@Column(name="user_followers")
	private Long followers;
//	@Column(name="user_following")
	private Long following ;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getProfile_pic() {
		return profile_pic;
	}

	public void setProfile_pic(String profile) {
		this.profile_pic = profile;
	}

	public Long getFollowers() {
		return followers;
	}

	public void setFollowers(Long followers) {
		this.followers = followers;
	}

	public Long getFollowing() {
		return following;
	}

	public void setFollowing(Long following) {
		this.following = following;
	}

	
}
